<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

    Route::middleware(['auth:web'])->group(function(){
        Route::get('/', 'HomeController@index')->name('web.index');
    
        //Siswa
        Route::get('siswa','SiswaController@index')->name('siswa.index');
        Route::get('tambah-siswa','SiswaController@create')->name('siswa.create');
        Route::post('tambah-siswa', 'SiswaController@store')->name('siswa.store');
        Route::get('siswa/{siswa}/detail', 'SiswaController@show')->name('siswa.show');
        Route::get('siswa/{siswa}/ubah', 'SiswaController@edit')->name('siswa.edit');
        Route::post('siswa/{siswa}/ubah','SiswaController@update')->name('siswa.update');
        Route::post('siswa/{siswa}/hapus', 'SiswaController@destroy')->name('siswa.destroy');
    
        //Kelas
        Route::get('kelas','KelasController@index')->name('kelas.index');
        Route::get('tambah-kelas','KelasController@create')->name('kelas.create');
        Route::post('tambah-kelas', 'KelasController@store')->name('kelas.store');
        Route::get('kelas/{kelas}/ubah', 'KelasController@edit')->name('kelas.edit');
        Route::post('kelas/{kelas}/ubah','KelasController@update')->name('kelas.update');
        Route::post('kelas/{kelas}/hapus', 'KelasController@destroy')->name('kelas.destroy');
    
        //Users
        Route::get('user','UserController@index')->name('user.index');
        Route::get('tambah-user','UserController@create')->name('user.create');
        Route::post('tambah-user', 'UserController@store')->name('user.store');
        Route::get('user/{user}/ubah', 'UserController@edit')->name('user.edit');
        Route::post('user/{user}/ubah','UserController@update')->name('user.update');
        Route::post('user/{user}/hapus', 'UserController@destroy')->name('user.destroy');
    
        //Pembayaran SPP
        Route::get('transaksi-spp','TransaksiController@index')->name('spp.index');
        Route::get('transaksi-create/{siswa}','TransaksiController@create')->name('transaksi.create');
        Route::post('transaksi-create/{siswa}','TransaksiController@update')->name('transaksi.update');

    });