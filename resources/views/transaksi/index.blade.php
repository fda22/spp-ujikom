@extends('layouts.app')
@section('page-name','Pembayaran SPP')
@section('content')
<div class="page-header">
    <h1 class="page-title">
    @yield('page-name')
    </h1>
</div>
<div class="row">
    <div class="col-12">
        <div class="col-16">
            <div class="card">
                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    {{ $error }}<br>
                    @endforeach
                </div>
                @endif
                <div class="card-header">
                    <h3 class="card-title">Data Siswa</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-hover table-vcenter text-wrap">
                        <thead>
                            <tr>
                                <th class="w-1">No.</th>
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Bayar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa as $index => $item)
                            <tr>
                                <td><span class="text-muted">{{ $index+1 }}</span></td>
                                <td> {{ $item->NIS }}</td>
                                <td>
                                    <a href="{{ route('siswa.show', $item->id) }}" class="link-unmuted">
                                        {{ $item->nama }}
                                    </a>
                                </td>
                                <td>
                                    {{ $item->nama_kelas }}
                                </td>
                                <td>
                                    <a class="icon" href="{{ route('transaksi.create', $item->id) }}" title="edit item">
                                        <i class="fe fe-edit">Bayar</i> 
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-16">
            <div class="card">
                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    {{ $error }}<br>
                    @endforeach
                </div>
                @endif
                <div class="card-header">
                    <h3 class="card-title">Histori Transaksi</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-hover table-vcenter text-wrap">
                        <thead>
                            <tr>
                                <th class="w-1">No.</th>
                                <th>Tanggal</th>
                                <th>Siswa</th>
                                <th>Kelas</th>
                                <th>SPP Bulan</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach ($historispp as $item)
                            <tr>
                                <td><span class="text-muted">{{ $i++ }}</span></td>
                                <td>{{ $item->tanggal_history }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->nama_kelas }}</td>
                                <td>{{ $item->nama_bulan }}</td>
                                <td>{{ $item->keterangan }}</td>
                            </tr>
                            @endforeach
                            <?php $i++; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
        @endsection
        @section('css')
        <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
        <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
        color: black;
        }
        .select2{
        width: 100% !important;
        }
        </style>
        @endsection
        @section('js')
<script>
$(document).ready(function(){
  $("#siswa").click(function(){
    $("#SPP").show()
    $("#SPP1").hide();
  });
});
</script>
@endsection