@extends('layouts.app')
@section('page-name','Pembayaran SPP')
@section('content')
<div class="page-header">
    <h1 class="page-title">
    @yield('page-name')
    </h1>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><b>{{$siswa->nama}}</b> {{$siswa->nama_kelas}}</h3>
            </div>
            @if(session()->has('msg'))
            <div class="card-alert alert alert-{{ session()->get('type') }}" id="message" style="border-radius: 0px !important">
                @if(session()->get('type') == 'success')
                <i class="fe fe-check mr-2" aria-hidden="true"></i>
                @else
                <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i>
                @endif
                {{ session()->get('msg') }}
            </div>
            @endif
            <div class="card-body">
                
                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    {{ $error }}<br>
                    @endforeach
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        @csrf
                        <div class="form-group" id="SPP">
                            <div class="row gutters-xs">
                                <div class="col">
                                    <label class="form-label">Juli</label>
                                    <div class="dropdown">
                                        <input type="hidden" name="id" value="{{ isset($siswa) ? $siswa->NIS : old('NIS') }}">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Juli : old('Juli') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <form action="{{ url("transaksi-create/$siswa->id") }}" method="POST">
                                                @csrf
                                                <button type="submit" class="dropdown-item" name="Juli" value="Lunas">Lunas</button>
                                                @csrf
                                                <button type="submit" class="dropdown-item" name="Juli" value="Belum Lunas">Belum Lunas</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">Agustus</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Agustus : old('Agustus') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">September</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->September : old('September') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">Oktober</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Oktober : old('Oktober') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">November</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->November : old('November') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">Desember</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Desember : old('Desember') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="SPP1" >
                            <div class="row gutters-xs">
                                <div class="col">
                                    <label class="form-label">Januari</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Januari : old('Januari') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">Febuari</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Febuari : old('Febuari') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">Maret</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Maret : old('Maret') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">April</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->April : old('April') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">Mei</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Mei : old('Mei') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label class="form-label">Juni</label>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ isset($siswa) ? $siswa->transaksi->Juni : old('Juni') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <button class="dropdown-item" type="button">Lunas</button>
                                            <button class="dropdown-item" type="button">Belum Lunas</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <div class="d-flex">
                    <a href="{{ route ('spp.index') }}" class="btn btn-link">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
        @endsection
        @section('css')
        <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
        <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
        color: black;
        }
        .select2{
        width: 100% !important;
        }
        </style>
        @endsection
        @section('js')
<script>
$(document).ready(function(){
  $("#siswa").click(function(){
    $("#SPP").show()
    $("#SPP1").hide();
  });
});
</script>
@endsection