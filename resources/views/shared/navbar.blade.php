<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="{{ route('web.index') }}" class="nav-link">
                            <i class="fe fe-home"></i> Dashboard
                        </a>
                    </li>
                    @if(Auth::user()->role == 'Petugas' || Auth::user()->role == 'SuperAdmin')
                    <li class="nav-item">
                        <a href="{{ route('spp.index') }}" class="nav-link">
                            <i class="fe fe-repeat"></i> Transaksi SPP
                        </a>
                    </li>
                    @if(Auth::user()->role == 'SuperAdmin')
                    <li class="nav-item">
                        <a href="{{ route('siswa.index') }}" class="nav-link">
                            <i class="fe fe-users"></i> Siswa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('kelas.index') }}" class="nav-link">
                            <i class="fe fe-box"></i>Kelas
                        </a>
                    </li>
                    </li>
                    <li class="nav-item">
                            <a href="{{ route('user.index') }}" class="nav-link">
                            <i class="fe fe-box"></i> Pengguna
                        </a>
                    </li>
                    @endif
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>