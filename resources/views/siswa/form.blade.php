@extends('layouts.app')

@section('site-name','Sistem Informasi SPP')
@section('page-name', (isset($siswa) ? 'Ubah Siswa' : 'Siswa Baru'))

@section('content')
<div class="row">
    <div class="col-8">
        <form action="{{ (isset($siswa) ? route('siswa.update', $siswa->id) : route('siswa.create')) }}" method="post" class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Siswa</h3>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    {{ $error }}<br>
                    @endforeach
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        @csrf
                        <div class="form-group">
                            <div class="row gutters-xs">
                                <div class="col-6">
                                    <label class="form-label">NISN</label>
                                    <input type="number" class="form-control" name="NISN" placeholder="NISN" value="{{ isset($siswa) ? $siswa->NISN : old('NISN') }}" required>
                                    <input type="hidden" class="form-control" name="idnya" placeholder="NISN" value="{{ isset($siswa) ? $siswa->id : old('id') }}" required>
                                </div>
                                <div class="col-6">
                                    <label class="form-label">NIS</label>
                                    <input type="number" class="form-control" name="NIS" placeholder="NIS" value="{{ isset($siswa) ? $siswa->NIS : old('NIS') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" value="{{ isset($siswa) ? $siswa->nama : old('nama') }}" required>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Alamat</label>
                            <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="{{ isset($siswa) ? $siswa->alamat : old('alamat') }}" required>
                        </div>
                        <div class="form-group">
                            <div class="row gutters-xs">
                                <div class="col-6">
                                    <label class="form-label">Kelas</label>
                                    <select id="select-beast" class="form-control custom-select" name="nama_kelas">
                                        @foreach($kelas as $item)
                                        <option value="{{ $item->nama_kelas }}" {{ isset($siswa) ? ($item->nama_kelas ? 'selected' : '') : '' }} > {{ $item->nama_kelas }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label class="form-label">No Telepon</label>
                                    <input type="number" class="form-control" name="no_telp" placeholder="Nomor Telp. Lengkap" value="{{ isset($siswa) ? $siswa->no_telp : old('no_telp') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row gutters-xs">
                                <div class="col-6">
                                    <label class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email" >
                                </div>
                                <div class="col-6">
                                    <label class="form-label">Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                    <input type="hidden" class="form-control" name="role" value="Siswa">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <div class="d-flex">
                    <a href="{{ url()->previous() }}" class="btn btn-link">Batal</a>
                    <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection