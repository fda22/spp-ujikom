@extends('layouts.app')

@section('page-name','Dashboard')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            @if(Auth::user()->role == 'Siswa')
            SPP {{$user->Siswa->nama}}
            @endif
            @if(Auth::user()->role == 'SuperAdmin' ||Auth::user()->role == 'Petugas')
            Dashboard
            @endif
        </h1>
    </div>
    <div class="row">
        @if(Auth::user()->role == 'SuperAdmin' ||Auth::user()->role == 'Petugas')
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Total Siswa</div>
                <div class="text-muted h2">{{$siswa}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Total Kelas</div>
                <div class="text-muted h2">{{$kelas}}</div>
                </div>
            </div>
        </div>
        @endif
        @if(Auth::user()->role == 'Siswa')
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Juli</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Juli}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Agustus</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Agustus}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">September</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->September}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Oktober</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Oktober}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">November</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->November}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Desember</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Desember}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Januari</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Januari}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Febuari</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Febuari}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Maret</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Maret}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">April</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->April}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Mei</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Mei}}</div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-lg-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                <div class="h3 m-0">Juni</div><br>
                <div class="text-muted h3">{{$user->Siswa->Transaksi->Juni}}</div>
                </div>
            </div>
        </div>
        @endif
        </div>
    </div>
@endsection