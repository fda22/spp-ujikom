<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $siswa = DB::table('siswa')->get();
        $totalsiswa = count($siswa);
        $kelas = DB::table('kelas')->get();
        $totalkelas = count($kelas);
        return view('home', [
            "user" => $user,
            "kelas" => $totalkelas,
            "siswa" => $totalsiswa
        ]);
    }
}
