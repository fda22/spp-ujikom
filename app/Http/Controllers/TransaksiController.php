<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Siswa;
use App\Histori;


class TransaksiController extends Controller
{
    public function index()
    {
    	$historispp = Histori::all();
    	$siswa = Siswa::all();
        return view('transaksi.index', [
            'siswa' => $siswa,
            'historispp' => $historispp
        ]);
    }
    public function create(Siswa $siswa)
    {
        return view('transaksi.form', [
            'siswa' => $siswa
        ]);
    }
    public function update(Request $request, $id)
    {
       
         $jj = DB::table('transaksi')->where('NIS',$request->id)->update([
        'Juli' => $request->Juli
        // 'Agustus' => $request->Agustus,
        // 'September' => $request->September,
        // 'Oktober' => $request->Oktober,
        // 'November' => $request->November,
        // 'Desember' => $request->Desember,
        // 'Januari' => $request->Januari,
        // 'Febuari' => $request->Febuari,
        // 'Maret' => $request->Maret,
        // 'April' => $request->April,
        // 'Mei' => $request->Mei,
        // 'Juni' => $request->Juni
         ]);
         // dd($jj);
        	return back()->with([
                'type' => 'success',
                'msg' => 'Transaksi Berhasil Masuk Histori'
            ]);
    }
}
