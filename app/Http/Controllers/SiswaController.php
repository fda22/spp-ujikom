<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Kelas;
use App\Siswa;

class SiswaController extends Controller
{
   public function index(Request $request) 
    {
       $siswa = Siswa::all();
        return view('siswa.index', [
            'siswa' => $siswa
        ]);
    }
    public function create()
    {
    	$kelas = Kelas::all();
        return view('siswa.form' , [
            'kelas' => $kelas
        ]);
    }
    public function store(Request $request)
    {
        // $request->validate([
        //     'nama' => 'required|max:255',
        //     'kk' => 'required|max:255',
        // ]);

        $nisn        = $request->input('NISN');
        $nis          = $request->input('NIS');
        $nama        = $request->input('nama');
        $kelas          = $request->input('nama_kelas');
        $alamat          = $request->input('alamat');
        $no_telp        = $request->input('no_telp');

        $email        = $request->input('email');
        $role        = $request->input('role');
        $password      = Hash::make($request->input('password'));
        $current_date_time = Carbon::now()->toDateTimeString();


        $data = array(
            'NISN' => $nisn,
            'NIS' => $nis,
            'nama' => $nama,
            'nama_kelas' => $kelas,
            'alamat' => $alamat,
            'no_telp' => $no_telp
         );
        $data1=array(
        	"name" => $nama,
            "password"=>$password,
            "email"=>$email,
            "role" => $role,
            "created_at"=>$current_date_time
        );
        $data2 = array(
            'NIS' => $nis
         );

			DB::table('siswa')->insert($data);
            DB::table('users')->insert([$data1]);
            DB::table('transaksi')->insert([$data2]);

            return redirect()->route('siswa.index')->with([
                'type' => 'success',
                'msg' => 'Siswa ditambahkan'
            ]);
    }

    public function edit(Siswa $siswa)
    {
        $kelas = Kelas::all();
        return view('siswa.form', [
            'siswa' => $siswa,
            'kelas' => $kelas
        ]);
    }

    public function update(Request $request)
    {

        DB::table('siswa')->where('id',$request->idnya)->update([
        'NISN' => $request->NISN,
        'NIS' => $request->NIS,
        'nama' => $request->nama,
        'nama_kelas' => $request->nama_kelas,
        'alamat' => $request->alamat,
        'no_telp' => $request->no_telp
    ]);

            return redirect()->route('siswa.index')->with([
                'type' => 'success',
                'msg' => 'Kelas diubah'
            ]);
    }

     public function show(Request $request, Siswa $siswa)
    {
    	$siswa = Siswa::all();
    	return view('siswa.show', [
    		'siswa' => $siswa
    	]);
    }


    public function destroy(Siswa $siswa)
    {
        if($siswa->delete()){
            return redirect()->route('siswa.index')->with([
                'type' => 'success',
                'msg' => 'Siswa dihapus'
            ]);
        }else{
            return redirect()->route('siswa.index')->with([
                'type' => 'danger',
                'msg' => 'Err.., Terjadi Kesalahan'
            ]);
        }
    }
}
