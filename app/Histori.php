<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Histori extends Model
{
     protected $table = 'history_spp';

    protected $fillable = [
        'tanggal_historry', 'nama', 'nama_kelas', 'nama_bulan', 'keterangan'
    ];
}
