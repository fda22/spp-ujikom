<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';

    protected $fillable = [
        'NIS', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember', 'Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni'
    ];

    function Siswa()
	{
		return $this->belongsTo(Siswa::class, 'NIS', 'NIS');
	}
}
