<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';

    protected $fillable = [
        'id','NISN', 'NIS', 'nama', 'nama_kelas', 'alamat', 'no_telp'
    ];

    function Transaksi()
	{
		return $this->hasOne(Transaksi::class, 'NIS', 'NIS');
	}
	function User()
	{
		return $this->belongsTo(User::class, 'NIS', 'NIS');
	} 

}
