<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_spp')->unique()->nullable();
            $table->enum('Juli', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Agustus', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('September', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Oktober', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('November', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Desember', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Januari', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Febuari', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Maret', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('April', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Mei', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
            $table->enum('Juni', ['Lunas', 'Belum Lunas'])->default('Belum Lunas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
