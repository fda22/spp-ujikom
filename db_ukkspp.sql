-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2020 at 09:55 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ukkspp`
--

-- --------------------------------------------------------

--
-- Table structure for table `history_spp`
--

CREATE TABLE `history_spp` (
  `id` int(11) NOT NULL,
  `tanggal_history` date NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nama_kelas` varchar(50) NOT NULL,
  `nama_bulan` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_spp`
--

INSERT INTO `history_spp` (`id`, `tanggal_history`, `nama`, `nama_kelas`, `nama_bulan`, `keterangan`) VALUES
(1, '2020-04-19', 'Farhan Dwi Anggara', 'XII RPL 2', 'Juli', 'Lunas');

--
-- Triggers `history_spp`
--
DELIMITER $$
CREATE TRIGGER `histori_spp` BEFORE UPDATE ON `history_spp` FOR EACH ROW BEGIN
    INSERT INTO history_spp
    SET
    nama = new.nama,
    nama_kelas = new.nama_kelas,
    tanggal_histori = NOW();
  END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_kelas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kompetensi_keahlian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `nama_kelas`, `kompetensi_keahlian`) VALUES
(2, 'XII RPL 1', 'Rekayasa Perangkat Lunak'),
(4, 'XII RPL 2', 'Rekayasa Perangkat Lunak'),
(7, 'XII RPL 3', 'Rekayasa Perangkat Lunak'),
(8, 'XII TKJ', 'Teknik Komputer Jaringan'),
(9, 'XII MM', 'Multimedia');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 2),
(4, '2020_04_15_112807_kelas', 3),
(5, '2020_04_15_140125_siswa', 4),
(7, '2020_04_18_130250_transaksi', 5);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `NISN` bigint(20) NOT NULL,
  `NIS` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_kelas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `NISN`, `NIS`, `nama`, `nama_kelas`, `alamat`, `no_telp`) VALUES
(2, 1021342, 1718117160, 'Farhan Dwi Anggara', 'XII RPL 2', 'Kp Bojong', '0888809582739');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(10) UNSIGNED NOT NULL,
  `NIS` bigint(20) DEFAULT NULL,
  `Juli` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Agustus` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `September` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Oktober` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `November` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Desember` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Januari` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Febuari` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Maret` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `April` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Mei` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  `Juni` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `NIS`, `Juli`, `Agustus`, `September`, `Oktober`, `November`, `Desember`, `Januari`, `Febuari`, `Maret`, `April`, `Mei`, `Juni`) VALUES
(1, 1718117160, 'Lunas', 'Lunas', 'Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas'),
(2, 1718117158, 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas', 'Belum Lunas');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('SuperAdmin','Petugas','Siswa','') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NIS` bigint(20) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `NIS`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Farhan Dwi Anggara', 'superadmin@gmail.com', NULL, '$2y$10$nxb5MkOA4ZRvpChym9j78uPjgkagN/hv.TjaKxEwy/y5aZtrSuBYa', 'SuperAdmin', NULL, 'QACiSYYwZJScCg2meqcpRPtd0QCsDyO5cXeePhK4BHY2UyryDAt0engp7MtR', '2020-04-12 05:19:14', '2020-04-12 05:19:14'),
(3, 'Farhan Dwi Anggara', 'siswa@gmail.com', NULL, '$2y$10$O/U5x7FMFRBIn5bsTgmcdelK4TogAi.Y11Wo61rqgtfD/an6SFBn2', 'Siswa', 1718117160, NULL, '2020-04-13 01:51:08', '2020-04-13 01:51:08'),
(4, 'petugas', 'petugas@gmail.com', NULL, '$2y$10$2/r3oq8sjY9Upz9MzoJble3ZmRufFbcTvT2WNpEZSQvgtq/EwZxai', 'Petugas', NULL, NULL, '2020-04-15 04:18:06', '2020-04-15 04:18:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history_spp`
--
ALTER TABLE `history_spp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama_kelas` (`nama_kelas`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `siswa_nis_unique` (`NIS`),
  ADD UNIQUE KEY `NISN` (`NISN`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaksi_id_spp_unique` (`NIS`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history_spp`
--
ALTER TABLE `history_spp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
